// Load plugins
const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');
const rename = require('gulp-rename');
const concat = require('gulp-concat');
const sourcemaps = require('gulp-sourcemaps');

// Styles
gulp.task('styles', function () {
    return gulp.src('./scss/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(concat('style.css'))
        .pipe(gulp.dest('css'))
        .pipe(cleanCSS())
        .pipe(rename({ suffix: '.min' }))
        .pipe(gulp.dest('./css'));
});

// Scripts
gulp.task('scripts', function () {
    return gulp.src('./js-source/*.js')
        .pipe(concat('script.js'))
        .pipe(minify())
        .pipe(gulp.dest('./js'));
});

// Images
gulp.task('images', function () {
    return gulp.src('image/*')
        .pipe(cache(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true })));
});

// Default task
gulp.task('default', function() {
    gulp.run('styles', 'scripts', 'images');
});

gulp.task('watch', function () {
    gulp.watch('js-source/*.js', ['scripts']);
    gulp.watch('scss/*', ['styles']);
});
